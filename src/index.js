import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import App from './App';
import * as serviceWorker from './serviceWorker';
import SampleForm from "./Components/SampleForm";
import SampleTable from "./Components/SampleTable";
import RandomEvents from "./Components/RandomEvents";

ReactDOM.render(
    <React.StrictMode>
        <App/>
        <SampleForm/>
        <SampleTable/>
        <RandomEvents />
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
