import React, {Component} from "react";
import Table from "react-bootstrap/Table";
import {Container} from "react-bootstrap";

class SampleTable extends Component {
    render() {
        React.createElement("div");
        return (
            <div>
                <section>
                    <Container>
                        <h3>This is a Sample Table</h3>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td colSpan="2">Larry the Bird is something what I needed the most in my life</td>
                                <td>@twitter</td>
                            </tr>
                            </tbody>
                        </Table>
                    </Container>
                </section>
            </div>
        );
    }
}

export default SampleTable;