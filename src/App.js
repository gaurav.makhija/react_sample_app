/*
code by Gaurav Makhija */
import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
     <h2>Hey! This is a sample form created by Gaurav Makhija</h2>
    </div>
  );
}

export default App;
