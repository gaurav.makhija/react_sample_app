import React, {Component} from "react";
import {Button, Col, Container, Row} from "react-bootstrap";
import Form from "react-bootstrap/Form";

class SampleForm extends Component {
    render() {
        React.createElement('div');
        return (
            <div>
                <section>
                    <Container fluid={"xs"}>
                        <Row>
                            <Col xl={{span: 4, offset: 3}}>
                                <Form method="post" action="/submit">
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control type="text"/>
                                        <Form.Text></Form.Text>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Email</Form.Label>
                                        <Form.Control type="email"/>
                                        <Form.Text></Form.Text>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password"/>
                                        <Form.Text></Form.Text>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Confirm Password</Form.Label>
                                        <Form.Control type="password"/>
                                        <Form.Text></Form.Text>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Date Of Birth</Form.Label>
                                        <Form.Control type="date"/>
                                        <Form.Text></Form.Text>
                                    </Form.Group>

                                    <Button variant="primary" type="submit">Submit Details</Button>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}

export default SampleForm;